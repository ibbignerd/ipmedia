package com.ibbignerd.MediaControlsSSH;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/**
 * Swag test
 * 
 */
public class SSHManager {

    private static final Logger LOGGER
            = Logger.getLogger(SSHManager.class.getName());
    private JSch jschSSHChannel;
    private String strUserName;
    private String strConnectionIP;
    private final int intConnectionPort;
    private String strPassword;
    private Session sesConnection;
    private final int intTimeOut;
    private static GetSSHMC getSSHMC = new GetSSHMC();
    private MediaControlsSSH_UI mediaControls = null;

    private void doCommonConstructorActions(String userName, String password, String connectionIP, String knownHostsFileName) {
        jschSSHChannel = new JSch();

        try {
            jschSSHChannel.setKnownHosts(knownHostsFileName);
        } catch (JSchException jschX) {
            logError(jschX.getMessage());
        }

        strUserName = userName;
        strPassword = password;
        strConnectionIP = connectionIP;
        mediaControls = MediaControlsSSH_UI.getInstance();
    }

    public SSHManager(String userName, String password, String connectionIP, String knownHostsFileName) {
        doCommonConstructorActions(userName, password,
                connectionIP, knownHostsFileName);
        intConnectionPort = 22;
        intTimeOut = 60000;
    }

    public SSHManager(String userName, String password, String connectionIP, String knownHostsFileName, int connectionPort) {
        doCommonConstructorActions(userName, password, connectionIP,
                knownHostsFileName);
        intConnectionPort = connectionPort;
        intTimeOut = 10000;
    }

    public SSHManager(String userName, String password, String connectionIP, String knownHostsFileName, int connectionPort, int timeOutMilliseconds) {
        doCommonConstructorActions(userName, password, connectionIP,
                knownHostsFileName);
        intConnectionPort = connectionPort;
        intTimeOut = timeOutMilliseconds;
    }

    public String connect() {
        String errorMessage = "";

        try {
            sesConnection = jschSSHChannel.getSession(strUserName,
                    strConnectionIP, intConnectionPort);
            sesConnection.setPassword(strPassword);
            sesConnection.setConfig("StrictHostKeyChecking", "no");
            sesConnection.connect(intTimeOut);
            MediaControlsSSH_UI.debugLogString("Successful connection!");

        } catch (JSchException jschX) {
            errorMessage = jschX.getMessage();
            MediaControlsSSH_UI.debugLogString("Failed to connect with error: " + jschX.getMessage());
        }

        return errorMessage;
    }

    private String logError(String errorMessage) {
        if (errorMessage != null) {
            LOGGER.log(Level.SEVERE, "{0}:{1} - {2}",
                    new Object[]{strConnectionIP, intConnectionPort, errorMessage});
        }

        return errorMessage;
    }

    private String logWarning(String warnMessage) {
        if (warnMessage != null) {
            LOGGER.log(Level.WARNING, "{0}:{1} - {2}",
                    new Object[]{strConnectionIP, intConnectionPort, warnMessage});
        }

        return warnMessage;
    }

    public boolean isAlive() {
        return sesConnection.isConnected();
    }

    public String sendCommand(String command) {
        if (!command.contains("info") && !command.contains("isRadio") && !command.contains("app") && !command.contains("length") && !command.contains("elapsed")) {
            MediaControlsSSH_UI.debugLogString("Sending command: " + command);
        }
        StringBuilder outputBuffer = new StringBuilder();

        try {
            Channel channel = sesConnection.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            channel.connect();
            InputStream commandOutput = channel.getInputStream();
            int readByte = commandOutput.read();

            while (readByte != 0xffffffff) {
                outputBuffer.append((char) readByte);
                readByte = commandOutput.read();
            }

            channel.disconnect();
        } catch (IOException | JSchException ioX) {
            String message = ioX.getMessage();
            MediaControlsSSH_UI.debugLogString("Failed sending command: " + command + ".\n With error: " + message);
            if (message.contains("channel is not opened")) {
                mediaControls.close();
            } 
            return "unable to send";
        }
//        if (!command.contains("info") && !command.contains("isRadio") && !command.contains("app") && !command.contains("length") && !command.contains("elapsed")) {
//            MediaControlsSSH_UI.debugLogString("Output: " + outputBuffer.toString());
//        }
        if (outputBuffer.toString().contains("command not found")) {
            getSSHMC.setVisible(!getSSHMC.isVisible());
        }

        return outputBuffer.toString();
    }

    public void close() {
        sesConnection.disconnect();
        mediaControls.debugLogString("User disconnected from host.");
    }
}
