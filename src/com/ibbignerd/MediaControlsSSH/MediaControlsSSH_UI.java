package com.ibbignerd.MediaControlsSSH;

import java.awt.Rectangle;
import static java.awt.event.KeyEvent.VK_ENTER;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author Nathan
 */
public class MediaControlsSSH_UI extends javax.swing.JFrame {

    private SSHManager sshInstance;
    public final String version = "Version: 0.6.2";
    private String host;
    private String password = "alpine";
    private String[] songInfo;
    private int port = 22;
    private String oldNowPlayingName = "nan";
    private String nowPlayingName = "nan";
    private String oldisRadio = "nan";
    private String isRadio = "nan";
    public static String songTitle = "nan";
    public static String songArtist = "nan";
    public boolean showNotifications = true;
    public String notifPos = "0";
    public static boolean saveIP = true;
    public static String lastIP = "";
    public boolean updateCheck = false;
    Timer timer;
    private static final int windowWidth = 258;
    private static DeveloperConsole_UI devConsole;
    private ArrayList<String> updateText = new ArrayList<>();
    private final static String thinChars = "[^iIlt1\\(\\)\\.,']";
    private static Update_UI updatePane;
    private static Notification_UI notifUI;
    private static GetSSHMC getSSHMC;
    private static Settings_UI settings;
    private static MediaControlsSSH_UI instance;
    public static SettingsManager settingsManager;
    private boolean timerRunning = false;

    /**
     * Creates new form MediaControlsSSH_UI
     */
    public MediaControlsSSH_UI() {
        initComponents();
        layout(true, false, false, false, false, false, false, false, 200);
        labelVersionNumber.setText(version);
    }

    public static MediaControlsSSH_UI getInstance() {
        return instance;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelMain = new javax.swing.JPanel();
        panelConnection = new javax.swing.JPanel();
        hostName = new javax.swing.JTextField();
        labelHostName = new javax.swing.JLabel();
        labelPass = new javax.swing.JLabel();
        PassVal = new javax.swing.JPasswordField();
        buttonConnect = new javax.swing.JButton();
        panelSongInfo = new javax.swing.JPanel();
        labelTitle = new javax.swing.JLabel();
        labelArtist = new javax.swing.JLabel();
        labelAlbum = new javax.swing.JLabel();
        progressSong = new javax.swing.JProgressBar();
        labelTimeLeft = new javax.swing.JLabel();
        title = new javax.swing.JLabel();
        artist = new javax.swing.JLabel();
        album = new javax.swing.JLabel();
        panelMediaControls = new javax.swing.JPanel();
        buttonNext = new javax.swing.JButton();
        buttonRepeat = new javax.swing.JButton();
        buttonPlayPause = new javax.swing.JButton();
        buttonPrevious = new javax.swing.JButton();
        buttonShuffle = new javax.swing.JButton();
        panelVolume = new javax.swing.JPanel();
        buttonVolMute = new javax.swing.JButton();
        buttonVolMax = new javax.swing.JButton();
        volumeSlider = new javax.swing.JSlider();
        panelPandora = new javax.swing.JPanel();
        buttonPanDislike = new javax.swing.JButton();
        buttonPanLike = new javax.swing.JButton();
        paneliTunesRadio = new javax.swing.JPanel();
        buttoniTunesHate = new javax.swing.JButton();
        buttoniTunesWish = new javax.swing.JButton();
        buttoniTunesLike = new javax.swing.JButton();
        panelFooter = new javax.swing.JPanel();
        buttonDisconnect = new javax.swing.JButton();
        labelVersionNumber = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        menuLaunch = new javax.swing.JMenu();
        launchNowPlaying = new javax.swing.JMenuItem();
        launchMusic = new javax.swing.JMenuItem();
        launchPandora = new javax.swing.JMenuItem();
        launchSpotify = new javax.swing.JMenuItem();
        launchDownCast = new javax.swing.JMenuItem();
        launchTuneIn = new javax.swing.JMenuItem();
        launchVLC = new javax.swing.JMenuItem();
        launchSoundHound = new javax.swing.JMenuItem();
        launchRdio = new javax.swing.JMenuItem();
        menuControls = new javax.swing.JMenu();
        controlsBluetooth = new javax.swing.JCheckBoxMenuItem();
        controlsCellData = new javax.swing.JCheckBoxMenuItem();
        controlsWiFi = new javax.swing.JCheckBoxMenuItem();
        controlsInsomnia = new javax.swing.JCheckBoxMenuItem();
        controlsSleep = new javax.swing.JMenuItem();
        controlsFMP = new javax.swing.JMenuItem();
        menuOptions = new javax.swing.JMenu();
        optionsDebug = new javax.swing.JMenuItem();
        optionsUpdate = new javax.swing.JMenuItem();
        optionsSettings = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("IPMedia");
        setResizable(false);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        panelMain.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelMain.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                panelMainKeyPressed(evt);
            }
        });

        panelConnection.setBorder(javax.swing.BorderFactory.createTitledBorder("Connection"));

        hostName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hostNameActionPerformed(evt);
            }
        });

        labelHostName.setText("Host Name:");

        labelPass.setText("Password:");

        PassVal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PassValActionPerformed(evt);
            }
        });
        PassVal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PassValKeyPressed(evt);
            }
        });

        buttonConnect.setText("Connect To Device");
        buttonConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonConnectActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelConnectionLayout = new javax.swing.GroupLayout(panelConnection);
        panelConnection.setLayout(panelConnectionLayout);
        panelConnectionLayout.setHorizontalGroup(
            panelConnectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConnectionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelConnectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelConnectionLayout.createSequentialGroup()
                        .addComponent(labelPass)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(PassVal))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelConnectionLayout.createSequentialGroup()
                        .addComponent(labelHostName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(hostName, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(buttonConnect, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelConnectionLayout.setVerticalGroup(
            panelConnectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConnectionLayout.createSequentialGroup()
                .addGroup(panelConnectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hostName)
                    .addComponent(labelHostName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelConnectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PassVal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPass))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonConnect))
        );

        panelSongInfo.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder("Song Info")));

        labelTitle.setText("Title:");

        labelArtist.setText("Artist:");

        labelAlbum.setText("Album:");

        labelTimeLeft.setText("0:00");

        title.setText("(null)");

        artist.setText("(null)");

        album.setText("(null)");

        javax.swing.GroupLayout panelSongInfoLayout = new javax.swing.GroupLayout(panelSongInfo);
        panelSongInfo.setLayout(panelSongInfoLayout);
        panelSongInfoLayout.setHorizontalGroup(
            panelSongInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelSongInfoLayout.createSequentialGroup()
                .addGroup(panelSongInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelSongInfoLayout.createSequentialGroup()
                        .addGroup(panelSongInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelAlbum)
                            .addComponent(labelTitle)
                            .addComponent(labelArtist))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelSongInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(title, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(artist, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(album, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(panelSongInfoLayout.createSequentialGroup()
                        .addComponent(progressSong, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelTimeLeft)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(6, 6, 6))
        );
        panelSongInfoLayout.setVerticalGroup(
            panelSongInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSongInfoLayout.createSequentialGroup()
                .addGroup(panelSongInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelTitle)
                    .addComponent(title, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelSongInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelArtist, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(artist))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelSongInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelAlbum)
                    .addComponent(album))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelSongInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(progressSong, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTimeLeft, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        panelMediaControls.setBorder(javax.swing.BorderFactory.createTitledBorder("Media Controls"));

        buttonNext.setText(">>");
        buttonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNextActionPerformed(evt);
            }
        });

        buttonRepeat.setText("Repeat");
        buttonRepeat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRepeatActionPerformed(evt);
            }
        });

        buttonPlayPause.setText("Play/Pause");
        buttonPlayPause.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPlayPauseActionPerformed(evt);
            }
        });

        buttonPrevious.setText("<<");
        buttonPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPreviousActionPerformed(evt);
            }
        });

        buttonShuffle.setText("Shuffle");
        buttonShuffle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonShuffleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMediaControlsLayout = new javax.swing.GroupLayout(panelMediaControls);
        panelMediaControls.setLayout(panelMediaControlsLayout);
        panelMediaControlsLayout.setHorizontalGroup(
            panelMediaControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMediaControlsLayout.createSequentialGroup()
                .addGroup(panelMediaControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelMediaControlsLayout.createSequentialGroup()
                        .addComponent(buttonShuffle)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonRepeat))
                    .addGroup(panelMediaControlsLayout.createSequentialGroup()
                        .addComponent(buttonPrevious)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonPlayPause)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonNext)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelMediaControlsLayout.setVerticalGroup(
            panelMediaControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMediaControlsLayout.createSequentialGroup()
                .addGroup(panelMediaControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonShuffle)
                    .addComponent(buttonRepeat))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelMediaControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonPrevious)
                    .addComponent(buttonPlayPause)
                    .addComponent(buttonNext))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelVolume.setBorder(javax.swing.BorderFactory.createTitledBorder("Volume"));

        buttonVolMute.setText("Mute");
        buttonVolMute.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonVolMuteActionPerformed(evt);
            }
        });

        buttonVolMax.setText("Max");
        buttonVolMax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonVolMaxActionPerformed(evt);
            }
        });

        volumeSlider.setMaximum(16);
        volumeSlider.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                volumeSliderMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout panelVolumeLayout = new javax.swing.GroupLayout(panelVolume);
        panelVolume.setLayout(panelVolumeLayout);
        panelVolumeLayout.setHorizontalGroup(
            panelVolumeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVolumeLayout.createSequentialGroup()
                .addComponent(buttonVolMute)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(volumeSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonVolMax, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelVolumeLayout.setVerticalGroup(
            panelVolumeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVolumeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(buttonVolMute, javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(buttonVolMax))
            .addComponent(volumeSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        panelPandora.setBorder(javax.swing.BorderFactory.createTitledBorder("Pandora Controls"));

        buttonPanDislike.setText("Dislike");
        buttonPanDislike.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPanDislikeActionPerformed(evt);
            }
        });

        buttonPanLike.setText("Like");
        buttonPanLike.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPanLikeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelPandoraLayout = new javax.swing.GroupLayout(panelPandora);
        panelPandora.setLayout(panelPandoraLayout);
        panelPandoraLayout.setHorizontalGroup(
            panelPandoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPandoraLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(buttonPanDislike, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(buttonPanLike, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelPandoraLayout.setVerticalGroup(
            panelPandoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPandoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(buttonPanDislike)
                .addComponent(buttonPanLike))
        );

        paneliTunesRadio.setBorder(javax.swing.BorderFactory.createTitledBorder("iTunes Radio Controls"));

        buttoniTunesHate.setText("Hate");
        buttoniTunesHate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttoniTunesHateActionPerformed(evt);
            }
        });

        buttoniTunesWish.setText("Wish");
        buttoniTunesWish.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttoniTunesWishActionPerformed(evt);
            }
        });

        buttoniTunesLike.setText("Like");
        buttoniTunesLike.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttoniTunesLikeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout paneliTunesRadioLayout = new javax.swing.GroupLayout(paneliTunesRadio);
        paneliTunesRadio.setLayout(paneliTunesRadioLayout);
        paneliTunesRadioLayout.setHorizontalGroup(
            paneliTunesRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneliTunesRadioLayout.createSequentialGroup()
                .addComponent(buttoniTunesHate, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttoniTunesWish, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttoniTunesLike, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12))
        );
        paneliTunesRadioLayout.setVerticalGroup(
            paneliTunesRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneliTunesRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(buttoniTunesHate)
                .addComponent(buttoniTunesWish)
                .addComponent(buttoniTunesLike))
        );

        buttonDisconnect.setText("Disconnect");
        buttonDisconnect.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buttonDisconnectMouseClicked(evt);
            }
        });
        buttonDisconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDisconnectActionPerformed(evt);
            }
        });

        labelVersionNumber.setForeground(new java.awt.Color(51, 51, 255));
        labelVersionNumber.setText("Version: #.#.#");
        labelVersionNumber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelVersionNumberMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout panelFooterLayout = new javax.swing.GroupLayout(panelFooter);
        panelFooter.setLayout(panelFooterLayout);
        panelFooterLayout.setHorizontalGroup(
            panelFooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFooterLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelVersionNumber)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addComponent(buttonDisconnect)
                .addContainerGap())
        );
        panelFooterLayout.setVerticalGroup(
            panelFooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFooterLayout.createSequentialGroup()
                .addGroup(panelFooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonDisconnect)
                    .addComponent(labelVersionNumber))
                .addGap(0, 5, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelMainLayout = new javax.swing.GroupLayout(panelMain);
        panelMain.setLayout(panelMainLayout);
        panelMainLayout.setHorizontalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelFooter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPandora, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelConnection, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelSongInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(paneliTunesRadio, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(panelMediaControls, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelVolume, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(11, Short.MAX_VALUE))
        );
        panelMainLayout.setVerticalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelConnection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelSongInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelMediaControls, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelVolume, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelPandora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(paneliTunesRadio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelFooter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        menuLaunch.setText("Launch");

        launchNowPlaying.setText("NowPlaying");
        launchNowPlaying.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                launchNowPlayingActionPerformed(evt);
            }
        });
        menuLaunch.add(launchNowPlaying);

        launchMusic.setText("Music");
        launchMusic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                launchMusicActionPerformed(evt);
            }
        });
        menuLaunch.add(launchMusic);

        launchPandora.setText("Pandora");
        launchPandora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                launchPandoraActionPerformed(evt);
            }
        });
        menuLaunch.add(launchPandora);

        launchSpotify.setText("Spotify");
        launchSpotify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                launchSpotifyActionPerformed(evt);
            }
        });
        menuLaunch.add(launchSpotify);

        launchDownCast.setText("DownCast");
        launchDownCast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                launchDownCastActionPerformed(evt);
            }
        });
        menuLaunch.add(launchDownCast);

        launchTuneIn.setText("TuneIn Radio");
        launchTuneIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                launchTuneInActionPerformed(evt);
            }
        });
        menuLaunch.add(launchTuneIn);

        launchVLC.setText("VLC");
        launchVLC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                launchVLCActionPerformed(evt);
            }
        });
        menuLaunch.add(launchVLC);

        launchSoundHound.setText("Sound Hound");
        launchSoundHound.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                launchSoundHoundActionPerformed(evt);
            }
        });
        menuLaunch.add(launchSoundHound);

        launchRdio.setText("Rdio");
        launchRdio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                launchRdioActionPerformed(evt);
            }
        });
        menuLaunch.add(launchRdio);

        menuBar.add(menuLaunch);

        menuControls.setText("Controls");

        controlsBluetooth.setText("Bluetooth");
        controlsBluetooth.setToolTipText("Flipswitch");
        controlsBluetooth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                controlsBluetoothActionPerformed(evt);
            }
        });
        menuControls.add(controlsBluetooth);

        controlsCellData.setText("Cellular Data");
        controlsCellData.setToolTipText("Flipswitch");
        controlsCellData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                controlsCellDataActionPerformed(evt);
            }
        });
        menuControls.add(controlsCellData);

        controlsWiFi.setText("WiFi");
        controlsWiFi.setToolTipText("Flipswitch");
        controlsWiFi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                controlsWiFiActionPerformed(evt);
            }
        });
        menuControls.add(controlsWiFi);

        controlsInsomnia.setText("Insomnia");
        controlsInsomnia.setToolTipText("Flipswitch (Insomnia)");
        controlsInsomnia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                controlsInsomniaActionPerformed(evt);
            }
        });
        menuControls.add(controlsInsomnia);

        controlsSleep.setText("Sleep");
        controlsSleep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                controlsSleepActionPerformed(evt);
            }
        });
        menuControls.add(controlsSleep);

        controlsFMP.setText("Find My iPhone");
        controlsFMP.setToolTipText("Play \"Bell Tower\" & turn on cellular data");
        controlsFMP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                controlsFMPActionPerformed(evt);
            }
        });
        menuControls.add(controlsFMP);

        menuBar.add(menuControls);

        menuOptions.setText("Options");

        optionsDebug.setText("Debug Console");
        optionsDebug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optionsDebugActionPerformed(evt);
            }
        });
        menuOptions.add(optionsDebug);

        optionsUpdate.setText("Check for Update");
        optionsUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optionsUpdateActionPerformed(evt);
            }
        });
        menuOptions.add(optionsUpdate);

        optionsSettings.setText("Settings");
        optionsSettings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optionsSettingsActionPerformed(evt);
            }
        });
        menuOptions.add(optionsSettings);

        menuBar.add(menuOptions);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        setBounds(0, 0, 255, 575);
    }// </editor-fold>//GEN-END:initComponents

    private void hostNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hostNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_hostNameActionPerformed

    private void PassValActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PassValActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PassValActionPerformed

    private void buttonDisconnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDisconnectActionPerformed
        close();
    }//GEN-LAST:event_buttonDisconnectActionPerformed

    private void buttonDisconnectMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonDisconnectMouseClicked

    }//GEN-LAST:event_buttonDisconnectMouseClicked

    private void buttonPlayPauseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPlayPauseActionPerformed
        sendCommand("media p");
    }//GEN-LAST:event_buttonPlayPauseActionPerformed

    private void buttonShuffleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonShuffleActionPerformed
        sendCommand("media shuffle");
    }//GEN-LAST:event_buttonShuffleActionPerformed

    private void buttonRepeatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRepeatActionPerformed
        sendCommand("media repeat");
    }//GEN-LAST:event_buttonRepeatActionPerformed

    private void buttonPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPreviousActionPerformed
        sendCommand("media prev");
    }//GEN-LAST:event_buttonPreviousActionPerformed

    private void buttonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNextActionPerformed
        sendCommand("media next");
    }//GEN-LAST:event_buttonNextActionPerformed

    private void buttonVolMuteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonVolMuteActionPerformed
        sendCommand("media volmin");
        volumeSlider.setValue(0);
    }//GEN-LAST:event_buttonVolMuteActionPerformed

    private void buttonVolMaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonVolMaxActionPerformed
        sendCommand("media volmax");
        volumeSlider.setValue(16);
    }//GEN-LAST:event_buttonVolMaxActionPerformed

    private void buttonConnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonConnectActionPerformed
        startConnection();
        checkMediaCommands();
        if (updateCheck) {
            updateCheck();
        }
    }//GEN-LAST:event_buttonConnectActionPerformed

    private void buttonPanLikeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPanLikeActionPerformed
        sendCommand("activator send Zaid.PandoraRateUp");
    }//GEN-LAST:event_buttonPanLikeActionPerformed

    private void buttonPanDislikeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPanDislikeActionPerformed
        sendCommand("activator send Zaid.PandoraRateDown");
    }//GEN-LAST:event_buttonPanDislikeActionPerformed

    private void PassValKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PassValKeyPressed
        if (evt.getKeyCode() == VK_ENTER) {
            startConnection();
            checkMediaCommands();
            if (updateCheck) {
                updateCheck();
            }
        }
    }//GEN-LAST:event_PassValKeyPressed

    private void labelVersionNumberMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_labelVersionNumberMouseClicked
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Info_UI().setVisible(true);
            }
        });
    }//GEN-LAST:event_labelVersionNumberMouseClicked

    private void buttoniTunesWishActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttoniTunesWishActionPerformed
        sendCommand("media wish");
    }//GEN-LAST:event_buttoniTunesWishActionPerformed

    private void buttoniTunesHateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttoniTunesHateActionPerformed
        sendCommand("media hate");
    }//GEN-LAST:event_buttoniTunesHateActionPerformed

    private void buttoniTunesLikeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttoniTunesLikeActionPerformed
        sendCommand("media like");
    }//GEN-LAST:event_buttoniTunesLikeActionPerformed

    private void launchMusicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchMusicActionPerformed
        sendCommand("media launchMusic");
    }//GEN-LAST:event_launchMusicActionPerformed

    private void launchPandoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchPandoraActionPerformed
        sendCommand("media launchPan");
    }//GEN-LAST:event_launchPandoraActionPerformed

    private void launchSpotifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchSpotifyActionPerformed
        sendCommand("media launchSpot");
    }//GEN-LAST:event_launchSpotifyActionPerformed

    private void launchDownCastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchDownCastActionPerformed
        sendCommand("media launchDC");
    }//GEN-LAST:event_launchDownCastActionPerformed

    private void optionsDebugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionsDebugActionPerformed
        devConsole.setVisible(!devConsole.isVisible());
    }//GEN-LAST:event_optionsDebugActionPerformed

    private void optionsUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionsUpdateActionPerformed
        updateCheck();
    }//GEN-LAST:event_optionsUpdateActionPerformed

    private void panelMainKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_panelMainKeyPressed

    }//GEN-LAST:event_panelMainKeyPressed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed

    }//GEN-LAST:event_formKeyPressed

    private void launchNowPlayingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchNowPlayingActionPerformed
        sendCommand("media activateApp");
    }//GEN-LAST:event_launchNowPlayingActionPerformed

    private void launchTuneInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchTuneInActionPerformed
        sendCommand("media launchTuneInPro");
    }//GEN-LAST:event_launchTuneInActionPerformed

    private void launchVLCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchVLCActionPerformed
        sendCommand("media launchVLC");
    }//GEN-LAST:event_launchVLCActionPerformed

    private void launchSoundHoundActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchSoundHoundActionPerformed
        sendCommand("media launchSH");
    }//GEN-LAST:event_launchSoundHoundActionPerformed

    private void launchRdioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchRdioActionPerformed
        sendCommand("media launchRdio");
    }//GEN-LAST:event_launchRdioActionPerformed

    private void controlsBluetoothActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_controlsBluetoothActionPerformed
        if (controlsBluetooth.isSelected()) {
            sendCommand("activator send switch-on.com.a3tweaks.switch.bluetooth");
        } else {
            sendCommand("activator send switch-off.com.a3tweaks.switch.bluetooth");
        }
    }//GEN-LAST:event_controlsBluetoothActionPerformed

    private void controlsCellDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_controlsCellDataActionPerformed
        if (controlsCellData.isSelected()) {
            sendCommand("activator send switch-on.com.a3tweaks.switch.cellular-data");
        } else {
            sendCommand("activator send switch-off.com.a3tweaks.switch.cellular-data");
        }
    }//GEN-LAST:event_controlsCellDataActionPerformed

    private void controlsWiFiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_controlsWiFiActionPerformed
        if (controlsWiFi.isSelected()) {
            sendCommand("activator send switch-on.com.a3tweaks.switch.wifi");
        } else {
            sendCommand("activator send switch-off.com.a3tweaks.switch.wifi");
        }
    }//GEN-LAST:event_controlsWiFiActionPerformed

    private void controlsInsomniaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_controlsInsomniaActionPerformed
        if (controlsInsomnia.isSelected()) {
            sendCommand("activator send switch-on.com.malcolmhall.insomnia");
        } else {
            sendCommand("activator send switch-off.com.malcolmhall.insomnia");
        }
    }//GEN-LAST:event_controlsInsomniaActionPerformed

    private void controlsSleepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_controlsSleepActionPerformed
        sendCommand("activator send libactivator.system.sleepbutton");
    }//GEN-LAST:event_controlsSleepActionPerformed

    private void controlsFMPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_controlsFMPActionPerformed
        sendCommand("activator send switch-off.com.a3tweaks.switch.ringer");
        sendCommand("activator send switch-on.com.a3tweaks.switch.cellular-data");
        sendCommand("activator send activator send libactivator.ringtone.system:Bell Tower");
    }//GEN-LAST:event_controlsFMPActionPerformed

    private void optionsSettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionsSettingsActionPerformed
        settings.setVisible(!settings.isVisible());
    }//GEN-LAST:event_optionsSettingsActionPerformed

    private void volumeSliderMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_volumeSliderMouseReleased
        sendCommand("media vol" + volumeSlider.getValue());
    }//GEN-LAST:event_volumeSliderMouseReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MediaControlsSSH_UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MediaControlsSSH_UI mediaControlsSSH_UI = new MediaControlsSSH_UI();
                instance = mediaControlsSSH_UI;
                mediaControlsSSH_UI.setVisible(true);
                devConsole = new DeveloperConsole_UI();
                updatePane = new Update_UI();
                notifUI = new Notification_UI();
                getSSHMC = new GetSSHMC();
                settings = new Settings_UI();
                settingsManager = new SettingsManager();
                settingsManager.loadProp();

                if (saveIP) {
                    System.out.println("Setting IP from file");
                    hostName.setText(lastIP);
                }
            }
        });
    }

    public static void updateLogString(String message) {
        updatePane.getUpdateLog().setText(updatePane.getUpdateLog().getText() + "\n" + message);
    }

    public static void debugLogString(String message) {
        devConsole.getDebugOutput().setText(devConsole.getDebugOutput().getText() + "\n\n" + message);
    }

    private void layout(boolean connection, boolean songInfo, boolean shuffle, boolean mediaControls, boolean volume, boolean pandora, boolean iTunes, boolean dc, int height) {
        panelConnection.setVisible(connection);
        menuBar.setVisible(!connection);
        menuLaunch.setVisible(!connection);
        panelSongInfo.setVisible(songInfo);
        buttonShuffle.setVisible(shuffle);
        buttonRepeat.setVisible(shuffle);
        panelMediaControls.setVisible(mediaControls);
        panelVolume.setVisible(volume);
        panelPandora.setVisible(pandora);
        paneliTunesRadio.setVisible(iTunes);
        buttonDisconnect.setVisible(dc);
        if (pandora || iTunes) {
            buttonPrevious.setEnabled(false);
        } else {
            buttonPrevious.setEnabled(true);
        }
        Rectangle oldPos = getBounds();
        setBounds(oldPos.x, oldPos.y, windowWidth, height);
    }

    private boolean setUpConnectInfo() {
        if (hostName.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Host name is empty!");
            debugLogString("Host name is empty!");
            return false;
        }
        if (hostName.getText().contains(":")) {
            String[] tempHost;
            tempHost = hostName.getText().split(":");
            this.host = tempHost[0];
            try {
                this.port = Integer.parseInt(tempHost[1]);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(rootPane, "Port must be in a number format!");
                debugLogString("Port must be in a number format!");
                return false;
            }
        } else {
            this.host = hostName.getText();
        }
        this.password = new String(PassVal.getPassword());
        return true;
    }

    private void startConnection() {
        if (!setUpConnectInfo()) {
            debugLogString("Failed to set up connection info!");
            return;
        }
        String errorMessage = connect();
        if (errorMessage.contains("timeout")) {
            JOptionPane.showMessageDialog(null, "Connection timed out!");
            debugLogString("Connection Error: Connection timed out!");
        } else if (errorMessage.contains("Auth fail")) {
            JOptionPane.showMessageDialog(null, "Incorrect password!");
            debugLogString("Connection Error: Incorrect password!");
        } else if (errorMessage.contains("UnknownHost")) {
            JOptionPane.showMessageDialog(null, "Unknown Host!");
            debugLogString("Connection Error: Unknown Host!");
        } else if (errorMessage.contains("Connection refused")) {
            JOptionPane.showMessageDialog(null, "Connection refused!");
            debugLogString("Connection Error: Connection refused! ");
        } else if (errorMessage.contains("Network is unreachable")) {
            JOptionPane.showMessageDialog(null, "Network is unreachable!");
            debugLogString("Connection Error: Network cannot be reached.");
        }
        lastIP = host;
        settingsManager.saveProp();
    }

    private String connect() {
        debugLogString("Starting mobile connection with hostname: " + host + " and port " + port + ".");
        sshInstance = new SSHManager("mobile", password, host, "", port);
        String errorMessage = sshInstance.connect();
        if (!errorMessage.equals("")) {
            System.out.println(errorMessage);
            debugLogString("Error: " + errorMessage);
        } else {
            startTimer();
            layout(false, false, true, true, true, false, false, true, 310);
        }
        return errorMessage;
    }

    private void checkMediaCommands() {
        String error = sendCommand("[ -f /usr/bin/media ] && echo 1 || echo 0");
        System.out.println(error);
        if (error.contains("0")) {
            getSSHMC.setVisible(true);
        }
    }

    private void updateCheck() {
        updatePane.getUpdateLog().setText("");
        debugLogString("Checking for updates...");
        try {
            URL url = new URL("http://void-technologies.com/nathan/IPMedia/update.txt");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            url.openStream()));
            String line = in.readLine();
            int i = 0;
            while (line != null) {
                updateText.add(i, line);
                i++;
                line = in.readLine();
            }
        } catch (IOException ex) {
            debugLogString(ex.toString());
        }
        if (!updateText.get(1).equalsIgnoreCase(version)) {
            debugLogString("There is an update available");
            try {
                updatePane.updateURL = new URL(updateText.get(0));
                System.out.println(updatePane.updateURL);
            } catch (MalformedURLException ex) {
                debugLogString("Improper URL format!");
                Logger.getLogger(MediaControlsSSH_UI.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (int j = 1; j < updateText.size(); j++) {
                System.out.println(updateText.get(j));
                updateLogString(updateText.get(j));
            }
            updatePane.setVisible(!updatePane.isVisible());
        } else {
            JOptionPane.showMessageDialog(null, "IPMedia is up to date!");
            debugLogString("IPMedia is up to date!");
        }
    }

    private void notificationTimer() {
        //System.out.println("Starting Notification Timer at: " + notifPos);
        notifUI.display();
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        notifUI.fadeOut();
                    }
                },
                5000
        );

    }

    private void startTimer() {
        debugLogString("Timer Started");
        timer = new Timer();
        TimerTask task = new TimerTask() {
            public void run() {
                timerRunning = true;
                if (isAlive()) {
                    String text = sendCommand("media info");
                    if (text != null) {
                        parseInfo("songinfo", text);
                    } else {
                        close();
                    }
                    if (songInfo.length > 4) {
                        title.setText(ellipsis(songInfo[3].trim(), 27));
                        album.setText(ellipsis(songInfo[2].trim(), 27));
                        artist.setText(ellipsis(songInfo[1].trim(), 27));
                    }

                    if (!artist.getText().equals(songArtist) && !songArtist.equals("nan") && !artist.getText().contains("(null)") && showNotifications) {
                        songArtist = artist.getText();
                        songTitle = title.getText();
                        notificationTimer();
                    } else {
                        songArtist = artist.getText();
                        songTitle = title.getText();
                    }
                    oldNowPlayingName = nowPlayingName;
                    oldisRadio = isRadio;
                    isRadio = sendCommand("media isRadio");
                    nowPlayingName = sendCommand("media appName");

                    if (!isRadio.equals(oldisRadio) || !nowPlayingName.equals(oldNowPlayingName)) {
                        if (nowPlayingName.contains("Music")) {
                            if (isRadio.contains("1")) {//iTunesRadio
                                debugLogString("Switching layout for iTunes Radio");
                                layout(false, true, false, true, true, false, true, true, 470);
                            } else {//Music.app
                                debugLogString("Switching layout for Music.app");
                                layout(false, true, true, true, true, false, false, true, 430);
                            }
                        } else if (nowPlayingName.contains("Pandora")) {
                            debugLogString("Switching layout for Pandora");

                            layout(false, true, false, true, true, true, false, true, 470);
                        } else {
                            debugLogString("Setting universal layout");
                            layout(false, false, true, true, true, false, false, true, 310);
                        }
                    }
                    String length = sendCommand("media length");
                    String time = sendCommand("media elapsed");
                    if (!time.contains("nan")) {
                        double total = Double.parseDouble(length);
                        double elapsed = Double.parseDouble(time);
                        int percent = (int) ((elapsed / total) * 100);
                        long sec = (long) (total - elapsed);
                        long seconds = TimeUnit.SECONDS.toSeconds(sec) - (TimeUnit.SECONDS.toMinutes(sec) * 60);
                        long mins = TimeUnit.SECONDS.toMinutes(sec);
                        if (seconds >= 10) {
                            labelTimeLeft.setText(mins + ":" + seconds);
                        } else {
                            labelTimeLeft.setText(mins + ":0" + seconds);
                        }
                        progressSong.setValue(percent);
                    } else {
                        labelTimeLeft.setText("0:00");
                        progressSong.setValue(0);
                    }
                    Double d = Double.valueOf(sendCommand("media getVol").replace(" ", ""));
                    volumeSlider.setValue((int) (d * 16));
                }
            }
        };
        timer.scheduleAtFixedRate(task,
                0, 1000);
    }

    private boolean isAlive() {
        if (sshInstance.isAlive()) {
            connectionPanel(false);
            return true;
        } else {
            connectionPanel(true);
            return false;
        }
    }

    public static void rawToJpeg(byte[] bytes, File outputFile) {
        try {
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(bytes));
            ImageIO.write(img, "jpg", outputFile);
        } catch (IOException e) {

        }
    }

    private static int textWidth(String str) {
        return (int) (str.length() - str.replaceAll(thinChars, "").length() / 2);
    }

    public static String ellipsis(String text, int max) {
        if (textWidth(text) < max) {
            return text;
        }

        int end = text.lastIndexOf(' ', max - 3);

        if (end == -1) {
            return text.substring(0, max - 3) + "...";
        }

        int newEnd = end;
        do {
            end = newEnd;
            newEnd = text.indexOf(' ', end + 1);

            if (newEnd == -1) {
                newEnd = text.length();
            }

        } while (textWidth(text.substring(0, newEnd) + "...") < max);
        return text.substring(0, end) + "...";
    }

    private void parseInfo(String type, String in) {
        switch (type) {
            case "songinfo":
                in = in.replaceAll("Artist", "");
                in = in.replaceAll("Album", "");
                in = in.replaceAll("Title", "");
                in = in.replaceAll("Duration", "");
                songInfo = in.split(":");
                break;
        }
    }

    public String sendCommand(String command) {
        if (sshInstance == null || !sshInstance.isAlive()) {
            close();
            return "no session";
        }
        return sshInstance.sendCommand(command);
    }

    private void connectionPanel(boolean in) {
        hostName.setEnabled(in);
        PassVal.setEnabled(in);
        buttonConnect.setEnabled(in);
    }

    public static Document parse(InputStream is) {
        Document ret = null;
        DocumentBuilderFactory domFactory;
        DocumentBuilder builder;

        try {
            domFactory = DocumentBuilderFactory.newInstance();
            domFactory.setValidating(false);
            domFactory.setNamespaceAware(false);
            builder = domFactory.newDocumentBuilder();

            ret = builder.parse(is);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            debugLogString("Unable to load XML: " + ex);
            System.out.println("unable to load XML: " + ex);
        }
        return ret;
    }

    public void close() {
        if (sshInstance == null) {
            JOptionPane.showMessageDialog(null, "No current session to close!");
            debugLogString("No current session to close!");
            return;
        }
        if (timerRunning) {
            timer.cancel();
        }
        sshInstance.close();
        isAlive();
        title.setText("(null)");
        album.setText("(null)");
        artist.setText("(null)");
        oldisRadio = "";
        isRadio = "";
        oldNowPlayingName = "";
        nowPlayingName = "";
        layout(true, false, false, false, false, false, false, false, 200);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField PassVal;
    private javax.swing.JLabel album;
    private javax.swing.JLabel artist;
    private javax.swing.JButton buttonConnect;
    private javax.swing.JButton buttonDisconnect;
    private javax.swing.JButton buttonNext;
    private javax.swing.JButton buttonPanDislike;
    private javax.swing.JButton buttonPanLike;
    private javax.swing.JButton buttonPlayPause;
    private javax.swing.JButton buttonPrevious;
    private javax.swing.JButton buttonRepeat;
    private javax.swing.JButton buttonShuffle;
    private javax.swing.JButton buttonVolMax;
    private javax.swing.JButton buttonVolMute;
    private javax.swing.JButton buttoniTunesHate;
    private javax.swing.JButton buttoniTunesLike;
    private javax.swing.JButton buttoniTunesWish;
    private javax.swing.JCheckBoxMenuItem controlsBluetooth;
    private javax.swing.JCheckBoxMenuItem controlsCellData;
    private javax.swing.JMenuItem controlsFMP;
    private javax.swing.JCheckBoxMenuItem controlsInsomnia;
    private javax.swing.JMenuItem controlsSleep;
    private javax.swing.JCheckBoxMenuItem controlsWiFi;
    private static javax.swing.JTextField hostName;
    private javax.swing.JLabel labelAlbum;
    private javax.swing.JLabel labelArtist;
    private javax.swing.JLabel labelHostName;
    private javax.swing.JLabel labelPass;
    private javax.swing.JLabel labelTimeLeft;
    private javax.swing.JLabel labelTitle;
    private javax.swing.JLabel labelVersionNumber;
    private javax.swing.JMenuItem launchDownCast;
    private javax.swing.JMenuItem launchMusic;
    private javax.swing.JMenuItem launchNowPlaying;
    private javax.swing.JMenuItem launchPandora;
    private javax.swing.JMenuItem launchRdio;
    private javax.swing.JMenuItem launchSoundHound;
    private javax.swing.JMenuItem launchSpotify;
    private javax.swing.JMenuItem launchTuneIn;
    private javax.swing.JMenuItem launchVLC;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuControls;
    private javax.swing.JMenu menuLaunch;
    private javax.swing.JMenu menuOptions;
    private javax.swing.JMenuItem optionsDebug;
    private javax.swing.JMenuItem optionsSettings;
    private javax.swing.JMenuItem optionsUpdate;
    private javax.swing.JPanel panelConnection;
    private javax.swing.JPanel panelFooter;
    private javax.swing.JPanel panelMain;
    private javax.swing.JPanel panelMediaControls;
    private javax.swing.JPanel panelPandora;
    private javax.swing.JPanel panelSongInfo;
    private javax.swing.JPanel panelVolume;
    private javax.swing.JPanel paneliTunesRadio;
    private javax.swing.JProgressBar progressSong;
    private javax.swing.JLabel title;
    private javax.swing.JSlider volumeSlider;
    // End of variables declaration//GEN-END:variables
}
