package com.ibbignerd.MediaControlsSSH;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nathan
 */
public class SettingsManager {

    private MediaControlsSSH_UI mediaControls = null;
    String propFileName = ".IPMedia.properties";
    private final File propFile = new File(propFileName);
    Properties prop = new Properties();

    public SettingsManager() {
        mediaControls = MediaControlsSSH_UI.getInstance();
    }

    public void loadProp() {
        try {
            if (!propFile.exists()) {
                System.out.println("Creating new file " + propFile.getName());
                propFile.createNewFile();
                //Files.setAttribute(propFile.toPath(), "dos:hidden", true);
                prop.setProperty("showNotifications", "true");
                prop.setProperty("notifPos", "0");
                prop.setProperty("saveIP", "true");
                prop.setProperty("lastIP", "");
                prop.setProperty("updateCheck", "false");
                saveProp();
            }
            Files.setAttribute(propFile.toPath(), "dos:hidden", false);
            try (FileInputStream fis = new FileInputStream(propFile)) {
                prop.load(fis);
                fis.close();
            }

            System.out.println("Setting variables from propFile");
            saveVars();
            //printVars();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(SettingsManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SettingsManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveProp() {
        try {
            Files.setAttribute(propFile.toPath(), "dos:hidden", false);
            try (FileOutputStream fos = new FileOutputStream(propFile)) {
                
                System.out.println("Saving variables to propFile");
                savePropVars();
                prop.store(fos, "IPMedia " + mediaControls.version);
                fos.close();
                Files.setAttribute(propFile.toPath(), "dos:hidden", true);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SettingsManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SettingsManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void printVars() {
        System.out.println("showNotifications=" + mediaControls.showNotifications);
        System.out.println("notifPos=" + mediaControls.notifPos);
        System.out.println("saveIP=" + MediaControlsSSH_UI.saveIP);
        System.out.println("lastIP=" + MediaControlsSSH_UI.lastIP);
        System.out.println("updateCheck=" + mediaControls.updateCheck);
    }

    private void saveVars() {
        mediaControls.showNotifications = Boolean.valueOf(prop.getProperty("showNotifications", "true"));
        mediaControls.notifPos = prop.getProperty("notifPos", "0");
        MediaControlsSSH_UI.saveIP = Boolean.valueOf(prop.getProperty("saveIP", "true"));
        MediaControlsSSH_UI.lastIP = prop.getProperty("lastIP", "");
        mediaControls.updateCheck = Boolean.valueOf(prop.getProperty("updateCheck", "false"));
    }
    
    private void savePropVars(){
        prop.setProperty("showNotifications", mediaControls.showNotifications + "");
        prop.setProperty("notifPos", mediaControls.notifPos + "");
        prop.setProperty("saveIP", MediaControlsSSH_UI.saveIP + "");
        prop.setProperty("lastIP", MediaControlsSSH_UI.lastIP);
        prop.setProperty("updateCheck", mediaControls.updateCheck + "");
    }

//    private static final Logger LOGGER
//            = Logger.getLogger(SSHManager.class.getName());
//    private final File propFile = new File(".IPMedia.properties");
//    private final Properties prop = new Properties();
//    private InputStream inFileStream = null;
//    private OutputStream outFileStream = null;
//    private MediaControlsSSH_UI mediaControls = null;
//
//    private void doCommon() {
//        mediaControls = MediaControlsSSH_UI.getInstance();
//    }
//
//    public boolean checkProp() {
//        if (propFile.exists()) {
//            System.out.println("propFile Exists");
//            return true;
//        } else {
//            System.out.println("propFile Does Not Exist");
//            return false;
//        }
//    }
//
//    public void createProp() {
//        System.out.println("Create Prop file");
//        OutputStream output = null;
//
//        try {
//            output = new FileOutputStream(propFile);
//            
//            prop.setProperty("showNotifications", "true");
//            prop.setProperty("notifPos", "0");
//            prop.setProperty("saveIP", "true");
//            prop.setProperty("lastIP", "");
//            prop.setProperty("updateCheck", "false");
//
//            prop.store(output, null);
//        } catch (IOException io) {
//            io.printStackTrace();
//        } finally {
//            if (output != null) {
//                try {
//                    output.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        try {
//            Files.setAttribute(propFile.toPath(), "dos:hidden", true);
//        } catch (IOException ex) {
//            Logger.getLogger(SettingsManager.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public void openProp() {
//        doCommon();
//        System.out.println("Opening propFile");
//        if (checkProp()) {
//            try {
//                inFileStream = new FileInputStream(propFile);
//                prop.load(inFileStream);
//            } catch (FileNotFoundException ex) {
//                Logger.getLogger(SettingsManager.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (IOException ex) {
//                Logger.getLogger(SettingsManager.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } else {
//            createProp();
//            openProp();
//        }
//    }
//
//    public String getProp(String key) {
//        String value = "";
//        value = prop.getProperty(key);
//        System.out.println(key + "=" + value);
//        return value;
//    }
//
//    public void setProp(String key, String value) {
//        prop.setProperty(key, value);
//    }
//
//    public void saveProp() {
//        OutputStream output = null;
//        try {
//            output = new FileOutputStream(propFile);
//            prop.store(output, null);
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(SettingsManager.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(SettingsManager.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//    }
//
//    public void closeProp() {
//        try {
//            inFileStream.close();
//        } catch (IOException ex) {
//            Logger.getLogger(SettingsManager.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
}
